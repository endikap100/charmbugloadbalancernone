#!/usr/bin/env python3
# Copyright 2021 endika
# See LICENSE file for licensing details.

import logging

from ops.charm import CharmBase
from ops.main import main
from ops.framework import StoredState
from ops.model import ActiveStatus, MaintenanceStatus

logger = logging.getLogger(__name__)


class NginxCharm(CharmBase):
    """Charm the service."""

    _stored = StoredState()

    def __init__(self, *args):
        super().__init__(*args)
        self.framework.observe(self.on.config_changed, self._on_config_changed)

    def make_pod_spec(self):
        config = self.framework.model.config
        ports = [{"name": self.framework.model.app.name, "containerPort": config["port"], "protocol": "TCP"}]

        spec = {
            "version": 3,
            "containers": [
                {
                    "name": self.framework.model.app.name,
                    "image": config["image"],
                    "ports": ports,
                },
            ],
        }

        return spec

    def _on_config_changed(self, _):
        if not self.unit.is_leader():
            self.unit.status = ActiveStatus("ready")
            return
        self.unit.status = MaintenanceStatus("Applying pod spec")
        pod_spec = self.make_pod_spec()
        self.framework.model.pod.set_spec(pod_spec)
        self.unit.status = ActiveStatus("ready")


if __name__ == "__main__":
    main(NginxCharm)
